comment convention
--------------------
comment can be placed after ; or begin
starts with the word "comment" and ends with (upto and including) semicolon(;)

comment can be placed after end and upto(and excluding) a semicolon(;)


the use of semicolon is to create a sequence of statements by piecing together single statements.


 begin D ; begin  D; S end; D ; ... ; D ; S ; S ; ... ; S end
 begin D; D; S; S; S begin D; S end end

 if x> 0 then put: z : = if y < x then x else y ;

 procedure I; S