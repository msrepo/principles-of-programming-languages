#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: forlistexample.alg		*/
/* Compiled at Sun Jul  8 10:56:28 2018		*/


#include	<stdio.h>
#include "forlistexample.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
 /* __for_body_0 declared at line 0*/
void ___for_body_0_41 (){ 
 outinteger (1, _i_40);

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_i_40=1;
___for_body_0_41 ();
_i_40=3;
___for_body_0_41 ();
_i_40=21;
___for_body_0_41 ();
_i_40=4;
___for_body_0_41 ();
_i_40=5;
___for_body_0_41 ();
}
}
