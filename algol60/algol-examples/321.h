#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: 321.alg		*/
/* Compiled at Sun Jul  8 09:51:21 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	int _doit_41 (int); /* doit declared at line 16*/
struct ___doit_41_rec {
char *__l_field;
int __res_val;
int n;
int _count_43; /* count declared at line 24*/
};
extern	char _odd_44 (struct ___doit_41_rec	*,int); /* odd declared at line 20*/
struct ___odd_44_rec {
struct ___doit_41_rec	*__l_field;
char __res_val;
int x;
};
extern int _i_40; /* i declared at line 41*/
extern int _n_40; /* n declared at line 41*/
extern int _val_40; /* val declared at line 41*/
extern int _max_40; /* max declared at line 41*/
extern int _NN_40; /* NN declared at line 41*/
