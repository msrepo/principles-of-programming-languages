#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: factorial.alg		*/
/* Compiled at Fri Jul  6 09:37:11 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _i_40; /* i declared at line 2*/
extern	int _factorial_41 (int); /* factorial declared at line 3*/
