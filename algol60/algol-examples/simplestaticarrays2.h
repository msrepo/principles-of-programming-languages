#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: simplestaticarrays2.alg		*/
/* Compiled at Sun Jul  8 10:28:17 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void space (int); /* space declared at line 22*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _A_40 []; // 
extern int _i_40; /* i declared at line 3*/
