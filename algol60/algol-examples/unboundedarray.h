#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: unboundedarray.alg		*/
/* Compiled at Sun Jul  8 10:40:00 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	void _squareupto_41 (char	*,int (*)(char *, int), int(*)(char *, int)); /* squareupto declared at line 2*/
struct ___squareupto_41_rec {
char *__l_field;
char	*Ln;
 int (*An)(char *, int);
int (*Vn)(char *, int);
int __dv0 [2 * 1 + DOPE_BASE];
int	*_A_43; /* A declared at line 4*/
int _i_43; /* i declared at line 5*/
};

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);
