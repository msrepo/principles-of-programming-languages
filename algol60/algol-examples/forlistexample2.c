#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: forlistexample2.alg		*/
/* Compiled at Sun Jul  8 11:00:09 2018		*/


#include	<stdio.h>
#include "forlistexample2.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
 /* __for_body_0 declared at line 0*/
void ___for_body_0_41 (){ 
 outinteger (1, _i_40);
newline (1);

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_i_40=3;
___for_body_0_41 ();
_i_40=7;
___for_body_0_41 ();
for (_i_40=11; ( _i_40- (16)) * sign ((double)1 )<= 0;_i_40 +=1)___for_body_0_41 ();
_i_40=(_i_40) / (2);
 while ((int)((_i_40) >= (1))) {___for_body_0_41 ();
_i_40=(_i_40) / (2); }
for (_i_40=2; ( _i_40- (32)) * sign ((double)_i_40 )<= 0;_i_40 +=_i_40)___for_body_0_41 ();
}
}
