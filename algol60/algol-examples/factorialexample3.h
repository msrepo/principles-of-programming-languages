#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: factorialexample3.alg		*/
/* Compiled at Sun Jul  8 10:07:14 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void space (int); /* space declared at line 22*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _i_40; /* i declared at line 2*/
extern	int _factorial_41 (char	*,int (*)(char *, int), int(*)(char *, int)); /* factorial declared at line 3*/
struct ___factorial_41_rec {
char *__l_field;
int __res_val;
char	*Ln;
 int (*An)(char *, int);
int (*Vn)(char *, int);
};

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);

//	specification for thunk
extern int A_jff_1A (char *, int);
extern int _jff_1A (char *, int);
