#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyname.alg		*/
/* Compiled at Sun Jul  8 10:50:20 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _A_40 []; // 
extern int _i_40; /* i declared at line 3*/
extern	int _sum_41 (char	*,int (*)(char *, int), int(*)(char *, int),int,int,char	*,int (*)(char *, int), int(*)(char *, int)); /* sum declared at line 4*/
struct ___sum_41_rec {
char *__l_field;
int __res_val;
char	*La;
 int (*Aa)(char *, int);
int (*Va)(char *, int);
int l;
int u;
char	*Lb;
 int (*Ab)(char *, int);
int (*Vb)(char *, int);
int _h_43; /* h declared at line 7*/
};

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);

//	specification for thunk
extern int A_jff_1A (char *, int);
extern int _jff_1A (char *, int);
