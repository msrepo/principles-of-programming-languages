#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: polynomialeqn.alg		*/
/* Compiled at Sun Jul  8 09:46:33 2018		*/


#include	<stdio.h>
#include "polynomialeqn.h"

//	Code for the global declarations

double _result_40; /* result declared at line 2*/
double _a_40; /* a declared at line 2*/
double _b_40; /* b declared at line 2*/
double _c_40; /* c declared at line 2*/
double _x_40; /* x declared at line 2*/
double  A_jff_0A (char *LP, double V){
return _a_40 = V;
}
double  _jff_0A (char *LP, int d){
return _a_40;
}
double  A_jff_1A (char *LP, double V){
return _b_40 = V;
}
double  _jff_1A (char *LP, int d){
return _b_40;
}
double  A_jff_2A (char *LP, double V){
return _c_40 = V;
}
double  _jff_2A (char *LP, int d){
return _c_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
outstring (1, "Enter three real numbers:");
inreal (0, LP, A_jff_0A, _jff_0A);
inreal (0, LP, A_jff_1A, _jff_1A);
inreal (0, LP, A_jff_2A, _jff_2A);
_x_40=(double)(2);
_result_40=(((_a_40) * ( __npow (_x_40, 2))) + ((_b_40) * (_x_40))) + (_c_40);
outstring (1, "The value of ax^2+bx+c:");
outreal (1, _result_40);
}
}
