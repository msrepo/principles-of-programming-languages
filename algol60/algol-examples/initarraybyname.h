#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: initarraybyname.alg		*/
/* Compiled at Sun Jul  8 10:43:42 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	int __dv0 [];
extern int _A_40 []; // 
extern int _i_40; /* i declared at line 3*/
extern	void _init_array_41 (int *, int	*); /* init_array declared at line 4*/
