#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: forexample.alg		*/
/* Compiled at Sun Jul  8 11:51:28 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	void ininteger (int,char	*,int (*)(char *, int), int(*)(char *, int)); /* ininteger declared at line 45*/
extern int _i_40; /* i declared at line 2*/

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);
