#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: polynomialeqn.alg		*/
/* Compiled at Sun Jul  8 09:46:33 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outreal (int,double); /* outreal declared at line 43*/
extern	void inreal (int,char	*,double (*)(char *, double), double(*)(char *, int)); /* inreal declared at line 46*/
extern double _result_40; /* result declared at line 2*/
extern double _a_40; /* a declared at line 2*/
extern double _b_40; /* b declared at line 2*/
extern double _c_40; /* c declared at line 2*/
extern double _x_40; /* x declared at line 2*/

//	specification for thunk
extern double A_jff_0A (char *, double);
extern double _jff_0A (char *, int);

//	specification for thunk
extern double A_jff_1A (char *, double);
extern double _jff_1A (char *, int);

//	specification for thunk
extern double A_jff_2A (char *, double);
extern double _jff_2A (char *, int);
