#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: example2.alg		*/
/* Compiled at Fri Jul  6 09:50:06 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _a_40; /* a declared at line 2*/
extern int _b_40; /* b declared at line 2*/
extern int _c_40; /* c declared at line 2*/
extern int _x_40; /* x declared at line 2*/
extern int _result_40; /* result declared at line 3*/
