#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: unboundedarray.alg		*/
/* Compiled at Sun Jul  8 10:40:00 2018		*/


#include	<stdio.h>
#include "unboundedarray.h"

//	Code for the global declarations

 /* squareupto declared at line 2*/
void _squareupto_41 (char	*Ln, int (*An)( char *, int), int (*Vn)(char *, int)){ 
 struct ___squareupto_41_rec local_data_squareupto;
struct ___squareupto_41_rec *LP = & local_data_squareupto;
LP -> Ln = Ln;
LP -> An = An;
LP -> Vn = Vn;

{ // code for block at line 4
(LP) ->  __dv0 [0] =  (1 *256) + sizeof (int);
(LP) ->  __dv0 [ DOPE_BASE +0] = 1;
(LP) ->  __dv0 [ DOPE_BASE +1] = ((LP) -> Vn)(((LP) -> Ln), 0);
__dv_init ((LP) ->  __dv0);
(LP) ->  _A_43 = (int *) __jff_allocate_array (&((LP) ->  __dv0), LP);
for ((LP) ->  _i_43=1; ( (LP) ->  _i_43- (((LP) -> Vn)(((LP) -> Ln), 0))) * sign ((double)1 )<= 0;(LP) ->  _i_43 +=1) *(int *) __jff_element_address ((LP) ->  _A_43, &((LP) ->  __dv0), 1, (LP) ->  _i_43)=((LP) ->  _i_43) * ((LP) ->  _i_43);
for ((LP) ->  _i_43=1; ( (LP) ->  _i_43- (((LP) -> Vn)(((LP) -> Ln), 0))) * sign ((double)1 )<= 0;(LP) ->  _i_43 +=1){ outinteger (1,  *(int *) __jff_element_address ((LP) ->  _A_43, &((LP) ->  __dv0), 1, (LP) ->  _i_43));
newline (1);
}
}
__deallocate (LP);

}
int  A_jff_0A (char *LP, int V){
fault (" no assignable object",13);
}
int  _jff_0A (char *LP, int d){
return 10;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_squareupto_41 (LP, A_jff_0A, _jff_0A);
}
}
