#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: nonlocalvariable.alg		*/
/* Compiled at Sun Jul  8 10:14:47 2018		*/


#include	<stdio.h>
#include "nonlocalvariable.h"

//	Code for the global declarations

int _h_40; /* h declared at line 2*/
 /* doit declared at line 3*/
void _doit_41 (){ 
 struct ___doit_41_rec local_data_doit;
struct ___doit_41_rec *LP = & local_data_doit;

{ // code for block at line 5
(LP) ->  _i_43=(LP) ->  _j_43=_h_40=0;
for ((LP) ->  _j_43=1; ( (LP) ->  _j_43- (100)) * sign ((double)1 )<= 0;(LP) ->  _j_43 +=1)_increment_44 (LP);
}

}
 /* increment declared at line 6*/
void _increment_44 (struct ___doit_41_rec *ELP){ 
 struct ___increment_44_rec local_data_increment;
struct ___increment_44_rec *LP = & local_data_increment;
LP -> __l_field = ELP;
(((struct ___doit_41_rec *)((((struct ___increment_44_rec *)(LP))) -> __l_field))) ->  _i_43=((((struct ___doit_41_rec *)((((struct ___increment_44_rec *)(LP))) -> __l_field))) ->  _i_43) + (1);
_h_40=(_h_40) + (1);
outstring (1, "i");
space (1);
outinteger (1, (((struct ___doit_41_rec *)((((struct ___increment_44_rec *)(LP))) -> __l_field))) ->  _i_43);
newline (1);

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_doit_41 ();
}
}
