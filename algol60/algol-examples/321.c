#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: 321.alg		*/
/* Compiled at Sun Jul  8 09:51:21 2018		*/


#include	<stdio.h>
#include "321.h"

//	Code for the global declarations

 /* doit declared at line 16*/
int _doit_41 (int n){ 
 struct ___doit_41_rec local_data_doit;
struct ___doit_41_rec *LP = & local_data_doit;
LP -> n= n;

{ // code for block at line 20
(LP) ->  _count_43=0;
L_do_43:; if (_odd_44 (LP, (LP) ->  n))
 (LP) ->  n=((3) * ((LP) ->  n)) + (1);
 
 else (LP) ->  n=((LP) ->  n) /(2);
(LP) ->  _count_43=((LP) ->  _count_43) + (1);
 if (((LP) ->  n) != (1))
 goto L_do_43;
 (LP ) -> __res_val = (LP) ->  _count_43;
}
return LP -> __res_val;

}
 /* odd declared at line 20*/
char _odd_44 (struct ___doit_41_rec *ELP,int x){ 
 struct ___odd_44_rec local_data_odd;
struct ___odd_44_rec *LP = & local_data_odd;
LP -> __l_field = ELP;
LP -> x= x;
 (LP ) -> __res_val = ((((struct ___doit_41_rec *)((((struct ___odd_44_rec *)(LP))) -> __l_field))) ->  n) != ((((((struct ___doit_41_rec *)((((struct ___odd_44_rec *)(LP))) -> __l_field))) ->  n) /(2)) * (2));
return LP -> __res_val;

}
int _i_40; /* i declared at line 41*/
int _n_40; /* n declared at line 41*/
int _val_40; /* val declared at line 41*/
int _max_40; /* max declared at line 41*/
int _NN_40; /* NN declared at line 41*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 16
_NN_40=1000;
outstring (1, "Hi!\n                n           iterations\n");
for (_i_40=1; ( _i_40- (_NN_40)) * sign ((double)1 )<= 0;_i_40 +=1){ _doit_41 (_i_40);
}
outstring (1, "...");
outstring (1, "\nnow looking for maxima:");
outstring (1, "\n                n           iterations\n");
_n_40=0;
_max_40= -(1);
for (_i_40=1; ( _i_40- (_NN_40)) * sign ((double)1 )<= 0;_i_40 +=1)
{ // code for block at line 0
L_do_47:;_n_40=(_n_40) + (1);
_val_40=_doit_41 (_n_40);
 if ((_val_40) <= (_max_40))
 goto L_do_47;
outinteger (1, _n_40);
outstring (1, " ");
outinteger (1, _val_40);
outstring (1, "\n");
_max_40=_val_40;
}
outstring (1, "...\ndone.");
}
}
