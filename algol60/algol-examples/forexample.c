#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: forexample.alg		*/
/* Compiled at Sun Jul  8 11:51:28 2018		*/


#include	<stdio.h>
#include "forexample.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
int  A_jff_0A (char *LP, int V){
return _i_40 = V;
}
int  _jff_0A (char *LP, int d){
return _i_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
ininteger (0, LP, A_jff_0A, _jff_0A);
_i_40= ( (_i_40) < (0)  )?  -(_i_40) : _i_40;
outinteger (1, _i_40);
}
}
