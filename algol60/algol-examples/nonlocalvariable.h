#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: nonlocalvariable.alg		*/
/* Compiled at Sun Jul  8 10:14:47 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void space (int); /* space declared at line 22*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _h_40; /* h declared at line 2*/
extern	void _doit_41 (); /* doit declared at line 3*/
struct ___doit_41_rec {
char *__l_field;
int _i_43; /* i declared at line 5*/
int _j_43; /* j declared at line 5*/
};
extern	void _increment_44 (struct ___doit_41_rec	*); /* increment declared at line 6*/
struct ___increment_44_rec {
struct ___doit_41_rec	*__l_field;
};
