#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: procinsideproc.alg		*/
/* Compiled at Sun Jul  8 10:18:08 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void space (int); /* space declared at line 22*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	void _doit_41 (); /* doit declared at line 2*/
struct ___doit_41_rec {
char *__l_field;
int _i_43; /* i declared at line 4*/
};
extern	void _increment_44 (struct ___doit_41_rec	*); /* increment declared at line 5*/
struct ___increment_44_rec {
struct ___doit_41_rec	*__l_field;
};
