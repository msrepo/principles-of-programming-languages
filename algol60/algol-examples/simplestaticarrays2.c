#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: simplestaticarrays2.alg		*/
/* Compiled at Sun Jul  8 10:28:17 2018		*/


#include	<stdio.h>
#include "simplestaticarrays2.h"

//	Code for the global declarations

int _A_40[100 -  -(25) +1]; /* A declared at line 2*/
int _i_40; /* i declared at line 3*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
for (_i_40= -(25); ( _i_40- (100)) * sign ((double)1 )<= 0;_i_40 +=1)_A_40 [_i_40- -(25)]=(_i_40) * (_i_40);
for (_i_40= -(25); ( _i_40- (100)) * sign ((double)1 )<= 0;_i_40 +=1){ outinteger (1, _i_40);
space (1);
outinteger (1, _A_40 [_i_40- -(25)]);
newline (1);
}
}
}
