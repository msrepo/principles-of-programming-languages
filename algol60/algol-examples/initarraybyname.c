#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: initarraybyname.alg		*/
/* Compiled at Sun Jul  8 10:43:42 2018		*/


#include	<stdio.h>
#include "initarraybyname.h"

//	Code for the global declarations

int __dv0 [2 * 1 + DOPE_BASE];
int _A_40[10 - 1 +1]; /* A declared at line 2*/
int _i_40; /* i declared at line 3*/
 /* init_array declared at line 4*/
void _init_array_41 (int *Db, int  *b){ 
 int _j_43;

{ // code for block at line 6
for (_j_43=1; ( _j_43- (10)) * sign ((double)1 )<= 0;_j_43 +=1) *(int *) __jff_element_address (b, Db, 1, _j_43)=(_j_43) * (_j_43);
}

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
__dv0 [0] =  (1 *256) + sizeof (int);
__dv0 [ DOPE_BASE +0] = 1;
__dv0 [ DOPE_BASE +1] = 10;
__dv_init (__dv0);
_init_array_41 (__dv0, _A_40);
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1){ outinteger (1, _A_40 [_i_40-1]);
newline (1);
}
}
}
