#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: factorial.alg		*/
/* Compiled at Fri Jul  6 09:37:11 2018		*/


#include	<stdio.h>
#include "factorial.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
 /* factorial declared at line 3*/
int _factorial_41 (int n){ 
 int __res_val;
__res_val =  ( (n) < (1)  )? 1 : (n) * (_factorial_41 ((n) - (1)));
return __res_val;

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
outstring (1, "Factorial Example\n");
outinteger (1, _factorial_41 (_i_40));
}
}
