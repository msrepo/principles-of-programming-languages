#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: example2.alg		*/
/* Compiled at Fri Jul  6 09:50:06 2018		*/


#include	<stdio.h>
#include "example2.h"

//	Code for the global declarations

int _a_40; /* a declared at line 2*/
int _b_40; /* b declared at line 2*/
int _c_40; /* c declared at line 2*/
int _x_40; /* x declared at line 2*/
int _result_40; /* result declared at line 3*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_a_40=1;
_b_40=1;
_c_40=1;
_x_40=2;
_result_40=(((_a_40) * ((__ipow(_x_40,2)))) + ((_b_40) * (_x_40))) + (_c_40);
outstring (1, "value of ax^2 + bx + c:");
outinteger (1, _result_40);
}
}
