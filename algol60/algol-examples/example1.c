#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: example1.alg		*/
/* Compiled at Fri Jul  6 09:39:17 2018		*/


#include	<stdio.h>
#include "example1.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
 /* fac declared at line 3*/
int _fac_41 (int n){ 
 int __res_val;
__res_val =  ( (n) < (1)  )? 1 : (n) * (_fac_41 ((n) - (1)));
return __res_val;

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
outstring (1, "Example 1\n");
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1){ outinteger (1, _i_40);
space (1);
outinteger (1, _fac_41 (_i_40));
newline (1);
}
}
}
