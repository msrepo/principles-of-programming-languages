#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyname.alg		*/
/* Compiled at Sun Jul  8 10:50:20 2018		*/


#include	<stdio.h>
#include "passbyname.h"

//	Code for the global declarations

int _A_40[10 - 1 +1]; /* A declared at line 2*/
int _i_40; /* i declared at line 3*/
 /* sum declared at line 4*/
int _sum_41 (char	*La, int (*Aa)( char *, int), int (*Va)(char *, int),int l,int u,char	*Lb, int (*Ab)( char *, int), int (*Vb)(char *, int)){ 
 struct ___sum_41_rec local_data_sum;
struct ___sum_41_rec *LP = & local_data_sum;
LP -> La = La;
LP -> Aa = Aa;
LP -> Va = Va;
LP -> l= l;
LP -> u= u;
LP -> Lb = Lb;
LP -> Ab = Ab;
LP -> Vb = Vb;

{ // code for block at line 7
(LP) ->  _h_43=0;
for (((LP) -> Aa) ((LP) -> La, (LP) ->  l); ( ((LP) -> Va)(((LP) -> La), 0)- ((LP) ->  u)) * sign ((double)1 )<= 0;((LP) -> Aa)((LP) -> La, ((LP) -> Va)(((LP) -> La), 0)+1))(LP) ->  _h_43=((LP) ->  _h_43) + (((LP) -> Vb)(((LP) -> Lb), 0));
 (LP ) -> __res_val = (LP) ->  _h_43;
}
return LP -> __res_val;

}
int  A_jff_0A (char *LP, int V){
return _i_40 = V;
}
int  _jff_0A (char *LP, int d){
return _i_40;
}
int  A_jff_1A (char *LP, int V){
return _A_40 [_i_40-1] = V;
}
int  _jff_1A (char *LP, int d){
return _A_40 [_i_40-1];
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1)_A_40 [_i_40-1]=(_i_40) * (_i_40);
outinteger (1, _sum_41 (LP, A_jff_0A, _jff_0A, 1, 10, LP, A_jff_1A, _jff_1A));
}
}
