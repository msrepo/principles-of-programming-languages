#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: simplestaticarrays.alg		*/
/* Compiled at Sun Jul  8 10:23:46 2018		*/


#include	<stdio.h>
#include "simplestaticarrays.h"

//	Code for the global declarations

int _A_40[10 - 1 +1]; /* A declared at line 2*/
int _i_40; /* i declared at line 3*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1)_A_40 [_i_40-1]=(_i_40) * (_i_40);
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1){ outinteger (1, _i_40);
space (1);
outinteger (1, _A_40 [_i_40-1]);
newline (1);
}
}
}
