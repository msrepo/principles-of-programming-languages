#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: forlistexample2.alg		*/
/* Compiled at Sun Jul  8 11:00:09 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _i_40; /* i declared at line 2*/
extern	void ___for_body_0_41 (); /* __for_body_0 declared at line 0*/
