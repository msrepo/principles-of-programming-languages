Design: SYNTACTIC STRUCTURE
	Fixed-format lexics was inherited from pseudo codess
	Ignoring blanks everywhere is a mistake
	The lack of reserved words is a mistake
	Algebraic notation was an important contribution
	Arithmetic operators have precedence
	Linear syntactic organization ( no nesting , no hierarchy)

Defence in Depth
	If an error gets through one line of defense ( syntactic checking),
	then it should be caught by the next line of defense (type checking).

Portability Principle
	Avoid features or facilities that are dependent on a particular computer or a small class of computers